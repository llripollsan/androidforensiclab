# Welcome!

The Android forensic environment provided below is a basic model and it is found equipped with several
software tools, APKs, scripts and packages. All of them are mainly thought to be used in Android mobile devices and
are open source in nature. Moreover, the forensic environment might help out with extracting different types of data
from those devices in a logical way. Besides, the operating system used is Linux, concretely the lubuntu distro, thus
some command-line skills might be necesary.
 
Download my customized Android forensic lab clicking on the link below:



## Requirements

* Computer sytem capable of virtualization technology.
* Virtualbox installed.
* Android mobile device (USB Debuggind enabled and superuser privileges recommended) along with its corresponding USB cable.


